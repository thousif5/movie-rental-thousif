const mongoose = require('mongoose');

const customerCollectionSchema = new mongoose.Schema({
  name: { type: String, required: true },
  isPremium: { type: Boolean, required: true },
  phone: { type: String, required: true },
});

const CustomerCollection = mongoose.model('customerCollection', customerCollectionSchema);

module.exports = CustomerCollection;
