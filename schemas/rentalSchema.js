const mongoose = require('mongoose');

const rentalCollectionSchema = new mongoose.Schema({
  customer: { type: mongoose.Schema.Types.ObjectId, ref: 'customerCollection' },
  movie: { type: mongoose.Schema.Types.ObjectId, ref: 'movies' },
  dateIssued: { type: Date, required: true },
  dateReturned: { type: Date, required: true },
  rentalFee: { type: Number, required: true },
});

const RentalCollection = mongoose.model('rentalCollection', rentalCollectionSchema);

module.exports = RentalCollection;
