const mongoose = require('mongoose');


// Schema
const moviesSchema = new mongoose.Schema({
  title: { type: String, required: true },
  genre: { type: String, required: true },
  numberInStock: { type: Number, required: true },
  dailyRentalRate: { type: Number, required: true },
});

// models
const Movies = mongoose.model('movies', moviesSchema);

module.exports = Movies;
