/* eslint-disable no-unused-vars */
/* eslint-disable no-prototype-builtins */
const express = require('express');

const winston = require('winston');

const bodyParser = require('body-parser');

const Movies = require('../schemas/moviesSchema');

const app = express();

const router = express.Router();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
}));

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'error.log' }),
  ],
});

router.get('/', (req, res) => {
  Movies.find().then((movie) => {
    const data = movie;
    const genreCollection = data.reduce((acc, val) => {
      if (!acc.hasOwnProperty('genre')) {
        acc.genre = [];
      }
      if (!acc.genre.includes(val.genre)) {
        acc.genre.push(val.genre);
      }
      return acc;
    }, {});
    res.json(genreCollection);
  }).catch((err) => {
    res.status(400).json('Unable to find');
  });
});

module.exports = router;
