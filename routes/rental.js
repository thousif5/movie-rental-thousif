/* eslint-disable no-prototype-builtins */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-vars */
const express = require('express');

const router = express.Router();
const bodyParser = require('body-parser');

const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const winston = require('winston');

const RentalCollection = require('../schemas/rentalSchema');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
}));

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'error.log' }),
  ],
});

// get rentals
router.get('/', (req, res) => {
  RentalCollection.find().populate('movie').populate('customer').then((item) => {
    // res.json(item);
    const uniqueId = [];
    item.forEach((val) => {
      // console.log(val.customer._id);
      if (!uniqueId.includes(val.customer._id)) {
        uniqueId.push(val.customer._id);
      }
    });
    const moviesUnique = uniqueId.reduce((accumulator, val) => {
      const customerMovie = item.reduce((acc, ele) => {
        if (ele.customer._id === val) {
          acc.customer = ele.customer;
          if (!acc.hasOwnProperty('movies')) {
            acc.movies = [];
          }
          acc.movies.push({
            movie: ele.movie,
            dateIssued: ele.dateIssued,
            dateReturned: ele.dateReturned,
            rentalFee: ele.rentalFee,
          });
        }
        return acc;
      }, {});
      accumulator.push(customerMovie);
      return accumulator;
    }, []);
    res.json(moviesUnique);
  })
    .catch((err) => {
      logger.log({
        level: 'info',
        message: err.message,
      });
      res.status(400).json('Unable to find');
    });
});

function validateRentals(body) {
  return joi.validate(body, {
    customer: joi.objectId().required(),
    movie: joi.objectId().required(),
    dateIssued: joi.date().required(),
    dateReturned: joi.date().required(),
    rentalFee: joi.number().required(),
  });
}

// create
router.post('/', (req, res) => {
  const {
    error,
  } = validateRentals(req.body);
  if (error) res.status(400).json(error.details[0].message);
  else {
    const myData = new RentalCollection(req.body);
    myData.save()
      .then((item) => {
        res.send('item saved to database');
      })
      .catch((err) => {
        logger.log({
          level: 'info',
          message: err.message,
        });
        res.status(400).json('unable to save');
      });
  }
});


module.exports = router;
