/* eslint-disable no-unused-vars */
const express = require('express');

const router = express.Router();
const bodyParser = require('body-parser');

const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const winston = require('winston');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
}));

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'error.log' }),
  ],
});

const customerCollection = require('../schemas/customerCollectionSchema');

// get customers
router.get('/', (req, res) => {
  customerCollection.find().then(customer => res.json(customer)).catch((err) => {
    logger.log({
      level: 'info',
      message: err.message,
    });
    res.status(400).json('Unable to find')});
});

function validateCustomer(body) {
  return joi.validate(body, {
    name: joi.string().required(),
    isPremium: joi.boolean().required(),
    phone: joi.string().required(),
  });
}

// create customer
router.post('/', (req, res) => {
  const {
    error,
  } = validateCustomer(req.body);
  if (error) res.status(400).json(error.details[0].message);
  else {
    // eslint-disable-next-line
    const myData = new customerCollection(req.body);
    myData.save()
      .then((item) => {
        res.send('item saved to database');
      })
      .catch((err) => {
        logger.log({
          level: 'info',
          message: err.message,
        });
        res.status(400).json('unable to save');
      });
  }
});

// get customer by ID
router.get('/:customerId', (req, res) => {
  customerCollection.find({
    _id: req.params.customerId,
  }).then(customer => res.json(customer)).catch((err) => {
    logger.log({
      level: 'info',
      message: err.message,
    });
    res.status(400).json('No data found');
  });
});

// update customer
router.put('/:customerId', (req, res) => {
  const {
    error,
  } = validateCustomer(req.body);
  if (error) res.status(400).json(error.details[0].message);
  else {
    customerCollection.findByIdAndUpdate(req.params.customerId, req.body, (err, res) => {
      if (err) {
        logger.log({
          level: 'info',
          message: err.message,
        });
        res.status(400).json('No data found');
      }
    });
    res.json(req.body);
  }
});

// delete customer
router.delete('/:customerId', (req, res) => {
  customerCollection.findByIdAndDelete(req.params.customerId, (err, res) => {
    if (err) {
      logger.log({
        level: 'info',
        message: err.message,
      });
      res.status(400).json('No data found');
    }
  });
  res.json('Deleted');
});

module.exports = router;
