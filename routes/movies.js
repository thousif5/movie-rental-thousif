/* eslint-disable no-shadow */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const express = require('express');

const winston = require('winston');

const bodyParser = require('body-parser');

const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const Movies = require('../schemas/moviesSchema');

const router = express.Router();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
}));

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'error.log' }),
  ],
});

// get movies
router.get('/', (req, res) => {
  Movies.find().then(movie => res.json(movie)).catch((err) => {
    logger.log({
      level: 'info',
      message: err.message,
    });
    res.status(400).json('Unable to find');
  });
});

// get movie by ID
router.get('/:movieId', (req, res) => {
  Movies.find({
    _id: req.params.movieId,
  }).then(movie => res.json(movie)).catch((err) => {
    logger.log({
      level: 'info',
      message: err.message,
    });
    res.status(400).json('No data found');
  });
});

function validateMovies(body) {
  return joi.validate(body, {
    title: joi.string().required(),
    genre: joi.string().required(),
    numberInStock: joi.number().required(),
    dailyRentalRate: joi.number().required(),
  });
}

// post new movie
router.post('/', (req, res) => {
  const {
    error,
  } = validateMovies(req.body);
  if (error) res.status(400).json(error.details[0].message);
  else {
    const myData = new Movies(req.body);
    myData.save()
      .then((item) => {
        res.send('item saved to database');
      })
      .catch((err) => {
        logger.log({
          level: 'info',
          message: err.message,
        });
        res.status(400).json('unable to save');
      });
  }
});

// update a movie
router.put('/:movieId', (req, res) => {
  const {
    error,
  } = validateMovies(req.body);
  if (error) res.status(400).json(error.details[0].message);
  else {
    Movies.findByIdAndUpdate(req.params.movieId, req.body, (err, res) => {
      if (err) {
        logger.log({
          level: 'info',
          message: err.message,
        });
        res.status(400).json('No data found');
      }
    });
    res.json(req.body);
  }
});

// delete a movie
router.delete('/:movieId', (req, res) => {
  Movies.findByIdAndDelete(req.params.movieId, (err, res) => {
    if (err) {
      logger.log({
        level: 'info',
        message: err.message,
      });
      res.status(400).json('No data found');
    }
  });
  res.json('Deleted');
});

module.exports = router;
