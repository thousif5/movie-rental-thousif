/* eslint-disable no-console */
const express = require('express');

const mongoose = require('mongoose');

const bodyParser = require('body-parser');

const movies = require('./routes/movies');

const genre = require('./routes/genre');

const customerCollection = require('./routes/customerCollection');

const rental = require('./routes/rental');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
}));

// database connection
mongoose.connect('mongodb://127.0.0.1:27017/movie-rental', {
  useNewUrlParser: true,
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('connected');
});

app.use('/api/movies', movies);
app.use('/api/genres', genre);
app.use('/api/customers', customerCollection);
app.use('/api/rentals', rental);

app.use('/', (req, res, next) => {
  res.status(500).send('Check parameters and try again');
  next();
});

app.listen(3000, () => {
  console.log('listening on 3000');
});
